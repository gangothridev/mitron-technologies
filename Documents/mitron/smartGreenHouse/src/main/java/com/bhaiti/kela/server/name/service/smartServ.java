package com.bhaiti.kela.server.name.service;

import java.util.Queue;

import com.bhaiti.kela.server.name.model.smartModel;

public interface smartServ {
	public void addValue(smartModel sm);
	public Queue<smartModel> getAllValues();

}

package com.bhaiti.kela.server.name;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartGreenHouseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartGreenHouseApplication.class, args);
	}

}
